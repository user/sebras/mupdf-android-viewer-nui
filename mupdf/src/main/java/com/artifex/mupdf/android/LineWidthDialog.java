package com.artifex.mupdf.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

public class LineWidthDialog extends PopupWindow
{
	private LineWidthChangedListener mListener;
	private LayoutInflater mInflater;

	private static final float mValues[] = {
		0.25f, 0.5f, 1, 1.5f, 2, 3, 4, 6, 8, 12, 18, 24,
	};

	public interface LineWidthChangedListener {
		void onLineWidthChanged(float value);
	}

	public LineWidthDialog(Context context, int width, LineWidthChangedListener listener) {
		super(context);

		mListener = listener;
		mInflater = LayoutInflater.from(context);

		ListView listView = (ListView)mInflater.inflate(R.layout.line_width_dialog, null);
		listView.setAdapter(new LineWidthAdapter());
		setContentView(listView);
		setWidth(width);
		setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	public class LineWidthAdapter extends BaseAdapter implements View.OnClickListener {
		public int getCount() { return mValues.length; }
		public Object getItem(int position) { return null; }
		public long getItemId(int position) { return 0; }

		public View getView(int position, View convertView, ViewGroup parent) {
			float lineWidth = mValues[position];

			if (convertView == null)
				convertView = mInflater.inflate(R.layout.line_width_item, null);
			convertView.setOnClickListener(this);
			convertView.setTag(lineWidth);

			TextView tv = (TextView)convertView.findViewById(R.id.value);
			if ((int)lineWidth == lineWidth)
				tv.setText(String.format("%d pt", (int)lineWidth));
			else
				tv.setText(String.format("%s pt", lineWidth));

			int h = (int)(mValues[position] * 3 / 2);
			if (h < 1)
				h = 1;
			View bar = convertView.findViewById(R.id.bar);
			bar.getLayoutParams().height = h;

			return convertView;
		}

		public void onClick(View v) {
			mListener.onLineWidthChanged((float)v.getTag());
			dismiss();
		}
	}
}
