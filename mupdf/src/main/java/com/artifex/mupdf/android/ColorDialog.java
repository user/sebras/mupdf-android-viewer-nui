package com.artifex.mupdf.android;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.GridView;
import android.widget.BaseAdapter;

public class ColorDialog extends PopupWindow
{
	private ColorChangedListener mListener;
	private LayoutInflater mInflater;
	private boolean mTransparent;

	private final int mColors[] = {
		0xFF000000, 0xFFFFFFFF, 0xFFD8D8D8, 0xFF808080, 0xFFEEECE1,
		0xFF1F497D, 0xFF0070C0, 0xFFC0504D, 0xFF9BBB59, 0xFF8064A2,
		0xFF4BACC6, 0xFFF79646, 0xFFFF0000, 0xFFFFFF00, 0xFFDBE5F1,
		0xFFF2DCDB, 0xFFEBF1DD, 0xFF00B050, 0x00000000
	};

	public interface ColorChangedListener {
		void onColorChanged(int color);
	}

	public ColorDialog(Context context, int width, boolean transparent, ColorChangedListener listener) {
		super(context);

		mListener = listener;
		mInflater = LayoutInflater.from(context);
		mTransparent = transparent;

		GridView gridView = (GridView)mInflater.inflate(R.layout.color_dialog, null);
		gridView.setAdapter(new ColorAdapter());
		setContentView(gridView);
		setWidth(width);
		setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	private class ColorAdapter extends BaseAdapter implements View.OnClickListener {
		public int getCount() { return mTransparent ? mColors.length : mColors.length - 1; }
		public Object getItem(int position) { return null; }
		public long getItemId(int position) { return 0; }

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null)
				convertView = mInflater.inflate(R.layout.color_cell, null);
			convertView.setOnClickListener(this);
			if (mColors[position] == Color.TRANSPARENT)
				convertView.setBackgroundResource(R.drawable.transparent_color_swatch);
			else
				convertView.setBackgroundColor(mColors[position]);
			convertView.setTag(mColors[position]);
			return convertView;
		}

		public void onClick(View v) {
			mListener.onColorChanged((int)v.getTag());
			dismiss();
		}
	}
}
